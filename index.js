function shipItem(item, weight, location) {
  let shippingFee = 0;
  if (weight <= 3) {
    shippingFee = 150;
  } else if (weight <= 5) {
    shippingFee = 280;
  } else if (weight <= 8) {
    shippingFee = 340;
  } else if (weight <= 10) {
    shippingFee = 410;
  } else if (weight > 10) {
    shippingFee = 560;
  } else {
    console.log("Please input a valid weight");
  }

  switch (location) {
    case "local":
      shippingFee += 0;
      break;
    case "international":
      shippingFee += 250;
      break;
    default:
      console.log("Please input a valid location");
      break;
  }

  return `The total shipping fee for a/an ${item} that will ship ${location} is ${shippingFee}`;
}

console.log(shipItem("bag", 3, "international"));

function Product(name, price, quantity) {
  this.name = name;
  this.price = price;
  this.quantity = quantity;
}

function Customer(name) {
  this.name = name;
  this.cart = [];
  this.addToCart = function (product) {
    this.cart.push(product);
    return `${product} is added to cart`;
  };
  this.removeToCart = function (product) {
    let productIndex = this.cart.indexOf(product);
    if (productIndex > 0) {
      this.cart.splice(productIndex, 1);
      return `${product} is removed from the cart`;
    } else {
      return `${product} is not found in the cart.`;
    }
  };
}

let palmolive = new Product("Palmolive", 2, 12);
console.log(palmolive);
let john = new Customer("John");
console.log(john);
console.log("Adding items to the cart");
console.log(john.addToCart("Water"));
console.log(john.addToCart("Orange"));
console.log(john.addToCart("Juice"));
console.log(john.cart);
console.log("Removing items from cart");
console.log(john.removeToCart("Apple"));
console.log(john.cart);
console.log(john.removeToCart("Orange"));
console.log(john.cart);
